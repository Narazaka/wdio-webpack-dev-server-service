const express = require("express");
const webpackDevMiddleware = require("webpack-dev-middleware");
var webpack = require("webpack");

class WDSLauncher {
  onPrepare(config) {
    if(!config.webpackConfig) {
        const path = require('path');
        config.webpackConfig = path.resolve(process.cwd(), 'webpack.config.js');
    }

    const webpackConfig = typeof config.webpackConfig === 'string' ? require(config.webpackConfig) : config.webpackConfig;
    const webpackPort = config.webpackPort || 8080;

    const app = express();
    const compiler = webpack(webpackConfig);

    if(webpackConfig.devServer && webpackConfig.devServer.historyApiFallback) {
        const options = typeof webpackConfig.devServer.historyApiFallback === 'boolean' ? {} : webpackConfig.devServer.historyApiFallback;

        const history = require('connect-history-api-fallback');
        app.use(history(options));
    }

    app.use(webpackDevMiddleware(compiler, { publicPath: webpackConfig.output.publicPath }));

    this.server = app.listen(webpackPort);
  }

  onComplete() {
    this.server.close()
  }
}

module.exports = WDSLauncher